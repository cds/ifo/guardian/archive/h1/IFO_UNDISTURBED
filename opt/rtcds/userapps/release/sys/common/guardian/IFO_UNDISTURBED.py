# -*- mode: python; tab-width: 4 -*-
#
# IFO Guardian "commissioning" node
#
# This is a simplified copy of the IFO top node to be used just during
# the commissioning period to indicate that the interferometer is in a
# "undisturbed" state for e.g. overnight noise evaluations etc.
#
# For this node the two requestable states are COMMISSIONING and
# UNDISTURBED.  If the interferometer is configured to auto-recover
# from a lock loss the node can be set to AUTO mode to automatically
# come back to undisturbed after the lock is recovered.  Otherwise set
# the node to MANAGED to require a re-request of UNDISTURBED after
# lock is recovered.

from guardian import GuardState

##################################################

request = 'COMMISSION'
nominal = 'UNDISTURBED'
GRD_MANAGER = 'OPERATOR'

##################################################

# list all nodes we want to monitor (probably at least ISC_LOCK but
# include others if needed)
# We arent going to officially manage these nodes, so we can avoid the OK bit
nodes = ['ISC_LOCK',]

##################################################

def node_fault():
    """Return True if any node in a fault condition.

    For this we will define a fault to be if the nodes are:
        in error


    """
    any_fault = False
    for node in nodes:
        fault = bool(ezca['GRD-{}_ERROR'.format(node)])
        any_fault |= fault
    return any_fault

def all_nodes_ok():
    """Check that all monitored nodes are in their desired state.

    """
    # We would like to just do a check for OK here, but during
    # commissioning all subordinate nodes are not necessarily expected
    # to be OK, so the main nodes we're monitoring might not be READY,
    # and therefore not themselves OK.  But that's ok for the point of
    # this node, so we instead just manually check for the conditions
    # we want to be satisfied (in this case INTENT and arrived).
    #
    # FIXME: guardian.manager needs to be modified to allow for direct
    # access to the INTENT (and ACTIVE/READY/etc) bits.
    check = True
    for node in nodes:
        # HACK: this is a hack to access this record that is not
        # currently properly exposed.  Don't replicate this!  Guardian
        # will be modified to expose this properly.
        ### HACKonHACK: TJS Nov 23 - The commented out code below didnt
        ### work as intended, but looking at the intent channel seems ok
        
        #if node._Node__get('INTENT') and \
        if ezca['GRD-{}_INTENT'.format(node)] and \
           ezca['GRD-{}_STATE'.format(node)] == ezca['GRD-{}_REQUEST'.format(node)]:
            pass
        else:
            notify("waiting for node: {}".format(node))
            check = False
    return check

##################################################

class INIT(GuardState):
    index = 0
    request = False

    def main(self):
        log("nodes monitored:")
        for node in nodes:
            log("  %s" % node)

    def run(self):
        if node_fault():
            return 'NODE_FAULT'
        else:
            return True


class NODE_FAULT(GuardState):
    index = 10
    request = False

    def run(self):
        if not node_fault():
            return 'WAITING_FOR_NODES'
        return False


class WAITING_FOR_NODES(GuardState):
    index = 20
    request = False

    def run(self):
        if node_fault():
            return 'NODE_FAULT'
        elif all_nodes_ok():
            return 'READY'
        else:
            return False


class READY(GuardState):
    index = 50
    request = False

    def run(self):
        if node_fault():
            return 'NODE_FAULT'
        elif not all_nodes_ok():
            return 'WAITING_FOR_NODES'
        return True


class COMMISSION(GuardState):
    index = 90

    def run(self):
        if node_fault():
            return 'NODE_FAULT'
        elif not all_nodes_ok():
            return 'WAITING_FOR_NODES'
        return True


class UNDISTURBED(GuardState):
    index = 100

    def run(self):
        if node_fault():
            return 'NODE_FAULT'
        elif not all_nodes_ok():
            return 'DROP_UNDISTURBED'
        return True


class DROP_UNDISTURBED(GuardState):
    index = 5
    request = False

    def run(self):
        if node_fault():
            return 'NODE_FAULT'
        else:
            return 'WAITING_FOR_NODES'


##################################################

edges = [
    ('INIT', 'WAITING_FOR_NODES'),
    ('WAITING_FOR_NODES', 'READY'),
    ('READY', 'UNDISTURBED'),
    ('READY', 'COMMISSION'),
    ('COMMISSION', 'UNDISTURBED'),
    ('UNDISTURBED', 'COMMISSION'),
    ('NODE_FAULT', 'WAITING_FOR_NODES'),
    ('DROP_UNDISTURBED', 'WAITING_FOR_NODES'),
]
